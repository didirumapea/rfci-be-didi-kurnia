# RFCI-BE-DIDI-KURNIA

Coding Interview RFCI-BE


Cara Penggunaan Soal No 1 : 

- git clone https://gitlab.com/didirumapea/rfci-be-didi-kurnia.git
- npm install 
- jalankan server (nodemon / npm start app.js)
- jalankan link http://localhost:PORT/sorting
- maka akan tampil di log server
- selesai.

Cara Penggunaan Soal No 2 (Soal No 2 sudah tergabung dengan soal No 1): 
- git clone https://gitlab.com/didirumapea/rfci-be-didi-kurnia.git
- npm install 
- jalankan server (nodemon / npm start app.js)
### langsung ke step ini jika soal No 1 telah di jalankan
- jalankan link http://localhost:PORT/counter
- maka akan tampil di log server dan file akan create di /server.log
- selesai.

Penjelasan soal No 3
- Kurang . pada env file
- kurang syntax dotenv.config
- untuk return datanya saya agak bingung ya, soalnya itu harus mereturn data, tetapi di .env filenya
tidak mencantumkan CLIENT_SECRET, OAUTH_URL ..
