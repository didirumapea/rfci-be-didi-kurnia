const express = require("express");
const authService = require("./src/services/authService");
const AuthCallbackService = require("./src/services/authCallbackService");
// const randomstring = require("randomstring");
const config = require("./src/config");
const bodyParser = require('body-parser');
const moment = require('moment')
const write = require('write');
const fs = require('fs');



const app = express();
app.use(bodyParser.json({
  limit: '50mb'
}));
app.use(bodyParser.urlencoded({
  extended: true,
  limit: '50mb',
  parameterLimit: 1000000
}));

app.get("/", (req, res) => {
  res.send('Hello World!')
});

app.get("/sorting", (req, res) => {
  let x = true;
  let num = 0;
  let raw = req.body.number;
  let arrayRaw = raw.split('');
  let temp = null
  let totalSwap = 0
  while (x){
    num++
    if (num-1 === arrayRaw.length){
      // end loop
      x = false
      res.json({status: 'ok'})
      console.log('Jumlah swap : '+ totalSwap)
    }else{
      if (arrayRaw[num-1] <= arrayRaw[num]){
      }else{
        if (arrayRaw[num] === undefined){
        }else{
          totalSwap++
          temp = arrayRaw[num-1]
          arrayRaw[num-1] = arrayRaw[num]
          arrayRaw[num] = temp
          console.log(`[${arrayRaw[num-1]}, ${arrayRaw[num]}] => ${arrayRaw}`)
          num = 0
        }
      }
    }
  }
});

app.post("/counter", (req, res) => {
  console.log(`[${moment().format()}] Success : POST http://localhost:${config.port}/ {"counter": ${req.body.counter}, "X-RANDOM": ${req.headers['x-random']}}`)
  fs.appendFileSync('server.log', `[${moment().format()}] Success : POST http://localhost:${config.port}/ {"counter": ${req.body.counter}, "X-RANDOM": "${req.headers['x-random']}"}\n`);
  res.status(201).json({
    success: true,
    result: `[${moment().format()}] Success : POST http://localhost:${config.port}/ {"counter": ${req.body.counter}, "X-RANDOM": ${req.headers['x-random']}}`,
    // data: rand,
  });
});

app.listen(config.port);
console.log(`App listening on http://localhost:${config.port}`);
